# Crop_ohos

**本项目是基于开源项目 AndroidCrop 进行鸿蒙化的移植和开发的，可以通过项目标签以及github地址（ https://github.com/jdamcd/android-crop ）追踪到原安卓项目版本**

#### 项目介绍

- 项目名称：开源图片裁剪组件
- 所属系列：鸿蒙的第三方组件适配移植
- 功能：将裁剪框在所选图片上移动或缩放，裁剪图片选定区域
- 项目移植状态：支持组件核心功能
- 调用差异：缺少调用系统相册以选择期待被裁减的图片
- 开发版本：sdk5，DevEco Studio2.1 beta3
- 项目作者和维护人：赵柏屹
- 联系方式：isrc_hm@iscas.ac.cn
- 原项目Doc地址：https://github.com/jdamcd/android-crop


#### 项目介绍

- 编程语言：Java 

#### 安装教程

在sdk4，DevEco Studio2.1 beta2下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用教程（可以参考本工程的entry模块）

该组件提供了一个自定义的裁剪框——可以在被裁减的图片范围内移动或缩放。并且提供了一个存放裁减图片的Image，以及用来实现页面跳转的button。以上能力全部封装在一个AbilitySlice——CropImageAbility中。

使用者可以通过如下的方式访问这个AbilitySlice：

```java
Crop.of(source, destination).asSquare().start(this.getAbility(),this);
```

其中source参数是一个int型的数据，表示被裁减的图片的位置，为ResourceTable中产生的值，如下图例子所示：

```java
public static final int Media_cropped = 0x1000003;
```

注：目前此组件被裁减的图片只能选择ResourceTable中配置好的，也就是在项目的media文件夹下面存在的。如有需求从其他位置导入，例如相册文件，可以自行增加额外功能。



跳转到组件提供的AbilitySlice后，组件AbilitySlice中的Button默认的跳转方式为跳转到MainAbility，如下所示。如需跳转到其他Ability或AbilitySlice，请自行阅读源代码并修改Button点击事件。

注：此点击事件存在于CropImageAbility文件中

```java
// 通过Intent中的OperationBuilder类构造operation对象，指定设备标识（空串表示当前设备）、应用包名、Ability名称
Operation operation = new Intent.OperationBuilder()
        .withDeviceId("")
        .withBundleName("com.huawei.mytestproject")
        .withAbilityName("com.huawei.mytestproject.MainAbility")
        .build();
// 把operation设置到intent中
intent.setOperation(operation);

//跳转
startAbility(intent);
```

跳转回主页面时intent中默认塞入了两个参数cropFlag，cropStatus。cropFlag为布尔型变量，表示是否裁剪成功。cropStatus为int型变量，表示裁剪状态。0为默认值，表示还未进行页面跳转，1表示取消裁剪，2表示裁剪成功，intent中有数据。可以参考如下示例j进行使用：

```java
if(intent.getIntParam("cropStatus",0) == 0){
    text.setText("欢迎使用");
}else if(intent.getIntParam("cropStatus",0) == 1){
    text.setText("剪裁取消");
}else if(intent.getIntParam("cropStatus",0) == 2){
    text.setText("剪裁成功");
}
```

裁剪后的裁剪结果判断可以参考如下代码:

```java
if(intent.getBooleanParam("cropFlag",false)){
    handleCrop(intent);
}
//处理剪裁结果
private void handleCrop(Intent result) {
	int resultImg = result.getIntParam("resultImg",0);
	if(resultImg != 0){
		PixelMap crpppp = CropUtil.getClippedPixelMap(this,resultImg).get();
		image.setPixelMap(crpppp);
	}
}
```

MainAbilitySlice的具体使用可以参考如下代码：

```java
public class MainAbilitySlice extends AbilitySlice {

    //定义一个图片
    Image image;
    //定义一个文本
    Text text;

    @Override
    public void onStart(Intent intent) {

        //重写onstart方法并加载布局文件
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_MainAbility_layout);

        //设置默认竖屏
        setDisplayOrientation(AbilityInfo.DisplayOrientation.PORTRAIT);

        //获取图片对象对应的component
        image = (Image) findComponentById(ResourceTable.Id_result_image);
        /*
        * 如果接收的cropFlag为true
        * 处理剪裁后的图片
        * 否则跳过
        */
        if(intent.getBooleanParam("cropFlag",false)){
            handleCrop(intent);
        }

        /* 自定义--获取文本对象对应的component
        * 根据intent里面的cropStatus来显示不同的文本
        * 0表示未接收到数据
        * 1表示剪裁取消
        * 2表示剪裁成功 有数据
        */
        text = (Text) findComponentById(ResourceTable.Id_text);
        if(intent.getIntParam("cropStatus",0) == 0){
            text.setText("欢迎使用");
        }else if(intent.getIntParam("cropStatus",0) == 1){
            text.setText("剪裁取消");
        }else if(intent.getIntParam("cropStatus",0) == 2){
            text.setText("剪裁成功");
        }

        //获取button对象对应的component
        Button button = (Button) findComponentById(ResourceTable.Id_button);
        // 设置button的属性及背景
        ShapeElement background = new ShapeElement();
        background.setRgbColor(new RgbColor(0, 125, 255));
        background.setCornerRadius(25);
        button.setBackground(background);
        if (button != null) {
            // 绑定点击事件
            button.setClickedListener(new Component.ClickedListener() {
                public void onClick(Component v) {
                    /*
                    * 开始裁剪
                    * 执行beginCrop函数
                    * 传进去的参数为图片在ResourceTable中注册的位置的int型的数据
                    */
                    beginCrop(ResourceTable.Media_cropped);
                }
            });
        }
    }

    //开始剪裁
    private void beginCrop(int source) {
        int destination = ResourceTable.Media_cropped;
        //执行函数跳转到裁剪页面
        Crop.of(source, destination).asSquare().start(this.getAbility(),this);
    }

    //处理剪裁结果
    private void handleCrop(Intent result) {
        int resultImg = result.getIntParam("resultImg",0);
        if(resultImg != 0){
            PixelMap crpppp = CropUtil.getClippedPixelMap(this,resultImg).get();
            image.setPixelMap(crpppp);
        }
    }

    @Override
    public void onActive() {super.onActive();}

    @Override
    public void onForeground(Intent intent) {super.onForeground(intent);
    }
}
```
#### 版本迭代

- v0.1.0-alpha

#### 版权和许可信息
crop_ohos经过[Apache License, version 2.0](http://www.apache.org/licenses/LICENSE-2.0)授权许可.