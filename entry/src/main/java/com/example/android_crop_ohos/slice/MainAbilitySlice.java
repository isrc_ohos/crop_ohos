/*
 *    Copyright 2016 SoundCloud
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.example.android_crop_ohos.slice;

import com.example.android_crop_ohos.ResourceTable;
import com.soundcloud.android.crop.Crop;
import com.soundcloud.android.crop.CropUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.ShapeElement;
import ohos.bundle.AbilityInfo;
import ohos.media.image.PixelMap;

/*
*
* @Author ZhaoBY
 */

public class MainAbilitySlice extends AbilitySlice {

    //定义一个图片
    Image image;
    //定义一个文本
    Text text;

    @Override
    public void onStart(Intent intent) {

        //重写onstart方法并加载布局文件
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_MainAbility_layout);

        //设置默认竖屏
        setDisplayOrientation(AbilityInfo.DisplayOrientation.PORTRAIT);

        //获取图片对象对应的component
        image = (Image) findComponentById(ResourceTable.Id_result_image);
        /*
        * 如果接收的cropFlag为true
        * 处理剪裁后的图片
        * 否则跳过
        */
        if(intent.getBooleanParam("cropFlag",false)){
            handleCrop(intent);
        }

        /* 自定义--获取文本对象对应的component
        * 根据intent里面的cropStatus来显示不同的文本
        * 0表示未接收到数据
        * 1表示剪裁取消
        * 2表示剪裁成功 有数据
        */
        text = (Text) findComponentById(ResourceTable.Id_text);
        if(intent.getIntParam("cropStatus",0) == 0){
            text.setText("欢迎使用");
        }else if(intent.getIntParam("cropStatus",0) == 1){
            text.setText("剪裁取消");
        }else if(intent.getIntParam("cropStatus",0) == 2){
            text.setText("剪裁成功");
        }


        //获取button对象对应的component
        Button button = (Button) findComponentById(ResourceTable.Id_button);
        // 设置button的属性及背景
        ShapeElement background = new ShapeElement();
        background.setRgbColor(new RgbColor(0, 125, 255));
        background.setCornerRadius(25);
        button.setBackground(background);
        if (button != null) {
            // 绑定点击事件
            button.setClickedListener(new Component.ClickedListener() {
                public void onClick(Component v) {
                    /*
                    * 开始裁剪
                    * 执行beginCrop函数
                    * 传进去的参数为图片在ResourceTable中注册的位置的int型的数据
                    */
                    beginCrop(ResourceTable.Media_cropped);
                }
            });
        }
    }

    //开始剪裁
    private void beginCrop(int source) {
        int destination = ResourceTable.Media_cropped;
        //执行函数跳转到裁剪页面
        Crop.of(source, destination).asSquare().start(this.getAbility(),this);
    }

    //处理剪裁结果
    private void handleCrop(Intent result) {
        int resultImg = result.getIntParam("resultImg",0);
        if(resultImg != 0){
            PixelMap crpppp = CropUtil.getClippedPixelMap(this,resultImg).get();
            image.setPixelMap(crpppp);
        }
    }


    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }


}
