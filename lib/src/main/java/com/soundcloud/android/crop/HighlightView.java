/*
 *    Copyright 2016 SoundCloud
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.soundcloud.android.crop;


import ohos.agp.components.Component;
import ohos.agp.components.ScaleInfo;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.*;
import ohos.app.Context;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import java.nio.file.WatchEvent;

/*
 * @Author ZhaoBY
 *
 * 这个累是与裁剪框相关
 * 提供了一个裁剪框
 * 并且可以初始化配置裁剪框的信息
 * 设置滑动、放大等等功能
 */
class HighlightView {

    private Paint mPaint;
    public Color mClipBoundColor;
    public boolean isAntiAlias;
    public float mStrokeWidth;
    private static Context context;

    public Component mclipbound;
    public RectFloat mclipRect;

    //构造函数
    public HighlightView(Context context , Component clipbound , RectFloat mclipRect){
        this.context = context;
        this.mPaint = new Paint();
        this.mClipBoundColor = Color.WHITE;
        this.isAntiAlias = true;
        this.mStrokeWidth = 5f;
        this.mclipRect = mclipRect;
        this.mclipbound = clipbound;
    }

    //获得裁剪框
    public Component getClipbound(HighlightView highlightView , RectFloat rectFloat){
        Component clipBound = new Component(context);
        //DrawTask
        Component.DrawTask drawRect = new Component.DrawTask() {
            @Override
            public void onDraw(Component component, Canvas canvas) {
                //draw裁剪框
                highlightView.draw(canvas, rectFloat);

                //component放大
                component.setScaleX(1f);
                component.setScaleY(1f);
            }
        };
        clipBound.addDrawTask(drawRect);
        return clipBound;
    }

    //画出裁剪框
    public void draw( Canvas canvas, RectFloat clipBoundRect){
        mPaint = new Paint();
        mPaint.setColor(mClipBoundColor);
        mPaint.setAntiAlias(isAntiAlias);
        mPaint.setStrokeWidth(mStrokeWidth);

        //画外边框
        float[] clipBoundPoints = getClipBoundPoints(clipBoundRect);
        canvas.drawLines(clipBoundPoints, mPaint);

        //画圆点
        canvas.drawCircle(clipBoundRect.left , clipBoundRect.top/2 + clipBoundRect.bottom/2 , 20 , mPaint);
        canvas.drawCircle(clipBoundRect.right , clipBoundRect.top/2 + clipBoundRect.bottom/2 , 20 , mPaint);
        canvas.drawCircle(clipBoundRect.left/2 + clipBoundRect.right/2 , clipBoundRect.top , 20 , mPaint);
        canvas.drawCircle(clipBoundRect.left/2 + clipBoundRect.right/2 , clipBoundRect.bottom , 20 , mPaint);

        //画中间线
        mPaint.setStrokeWidth(1);
        float xThird = (clipBoundRect.right - clipBoundRect.left) / 3;
        float yThird = (clipBoundRect.bottom - clipBoundRect.top) / 3;

        canvas.drawLine(new Point(clipBoundRect.left + xThird , clipBoundRect.top),
                new Point(clipBoundRect.left + xThird, clipBoundRect.bottom), mPaint);
        canvas.drawLine(new Point(clipBoundRect.left + xThird * 2, clipBoundRect.top),
                new Point(clipBoundRect.left + xThird * 2, clipBoundRect.bottom), mPaint);
        canvas.drawLine(new Point(clipBoundRect.left, clipBoundRect.top + yThird),
                new Point(clipBoundRect.right, clipBoundRect.top + yThird), mPaint);
        canvas.drawLine(new Point(clipBoundRect.left, clipBoundRect.top + yThird * 2),
                new Point(clipBoundRect.right, clipBoundRect.top + yThird * 2), mPaint);
    }

    //获得裁剪框的矩形大小
    public float[] getClipBoundPoints(RectFloat clipBoundRect){
        Point point1 = new Point(clipBoundRect.left,clipBoundRect.top);
        Point point2 = new Point(clipBoundRect.left,clipBoundRect.bottom);
        Point point3 = new Point(clipBoundRect.right,clipBoundRect.bottom);
        Point point4 = new Point(clipBoundRect.right,clipBoundRect.top);

        float[] pointsToDraw = {
                point1.position[0],point1.position[1], point2.position[0],point2.position[1],
                point2.position[0],point2.position[1], point3.position[0],point3.position[1],
                point3.position[0],point3.position[1], point4.position[0],point4.position[1],
                point4.position[0],point4.position[1], point1.position[0],point1.position[1]};
        return pointsToDraw;
    }

    //更新裁剪框
    public static void updateClipBound(HighlightView highlightView,Component component, RectFloat clipBoundRect) {

        Component.DrawTask mClipBoundUpdate = new Component.DrawTask() {
            @Override
            public void onDraw(Component component, Canvas canvas) {
                //draw裁剪框
                highlightView.draw(canvas, clipBoundRect);
            }
        };
        component.release();
        component.addDrawTask(mClipBoundUpdate);

    }

    //滑动监听
    public void setSlideListener(HighlightView highlightView , Component mClipBound){
        //初始化滑动监听
        mClipBound.setTouchEventListener(new Component.TouchEventListener() {
            //创建一个RectFloat用来记录滑动之后的位置
            RectFloat mScrolledClipBoundRect;

            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                //获得当前手指点击位置
                MmiPoint position = touchEvent.getPointerPosition(0);
                float x = position.getX();
                float y = position.getY();

                //获得当前裁剪框的宽和高
                float width = getClipBoundWidth();
                float height = getClipBoundHeight();

                //获得当前图片的位置
                int left = CropImageView.getPositionLeft();
                int top = CropImageView.getPositionTop();
                int right = CropImageView.getPositionRight();
                int bottom = CropImageView.getPositionBottom();

                //获得裁剪框位置
                RectFloat cropBoundRect = HighlightView.getCropBoundPositionRect();
                float cropBoundLeft = cropBoundRect.left;
                float cropBoundTop = cropBoundRect.top;
                float cropBoundRight = cropBoundRect.right;
                float cropBoundBottom = cropBoundRect.bottom;

                //判断裁剪框的位置，裁剪框不能超过图片的边界，并且点击事件必须在裁剪框内才可以移动裁剪框
                if((right > (x + width / 2)) &&
                        ((x - width / 2) > left) &&
                        (bottom > (y + height / 2)) &&
                        ((y - height / 2) > top) &&
                        (cropBoundRight > x) &&
                        (x > cropBoundLeft) &&
                        (cropBoundBottom > y) &&
                        (y > cropBoundTop))
                {
                    mScrolledClipBoundRect = new RectFloat(x - width / 2.0f, y - height / 2.0f, x + width / 2.0f, y + height / 2.0f);

                    HighlightView.updateClipBound(highlightView , mClipBound, mScrolledClipBoundRect);

                    CropImgAbility.mClipRect = mScrolledClipBoundRect;
                    return false;
                }

                return false;
            }
        });
    }

    //放大缩小监听
    public void setScaledListener(HighlightView clipBound , Component mClipBound){
        //初始化缩放监听
        mClipBound.setScaledListener(new Component.ScaledListener() {
            Point point1;
            Point point2;
            RectFloat mScaledRect;
            @Override
            public void onScaleStart(Component component, ScaleInfo scaleInfo) {
            }

            @Override
            public void onScaleUpdate(Component component, ScaleInfo scaleInfo) {
                float left = 0;
                float right = 0;
                float top = 0;
                float bottom = 0;
                float index = 0;
                if(scaleInfo.horizontalScale != 0){
                    index = CropImgAbility.mClipRect.right * (float) Math.sqrt((float) scaleInfo.horizontalScale)
                            - CropImgAbility.mClipRect.right/2 - CropImgAbility.mClipRect.left/2;
                    right = CropImgAbility.mClipRect.right * (float) Math.sqrt((float) scaleInfo.horizontalScale);
                    bottom = CropImgAbility.mClipRect.top/2 + CropImgAbility.mClipRect.bottom/2 + index;
                    left = CropImgAbility.mClipRect.right/2 + CropImgAbility.mClipRect.left/2 - index;
                    top = CropImgAbility.mClipRect.top/2 + CropImgAbility.mClipRect.bottom/2 - index;
                }else if(scaleInfo.verticalScale !=0){
                    index = CropImgAbility.mClipRect.bottom * (float)Math.sqrt((float) scaleInfo.verticalScale)
                            - CropImgAbility.mClipRect.bottom/2 - CropImgAbility.mClipRect.top/2;
                    bottom = CropImgAbility.mClipRect.bottom * (float)Math.sqrt((float) scaleInfo.verticalScale);
                    top = CropImgAbility.mClipRect.top/2 + CropImgAbility.mClipRect.bottom/2 - index;
                    left = CropImgAbility.mClipRect.right/2 + CropImgAbility.mClipRect.left/2 - index;
                    right = CropImgAbility.mClipRect.right/2 + CropImgAbility.mClipRect.left/2 + index;
                }

                /*
                * 设置判断 剪裁框不能超过图片的范围
                * 也不能剪裁框右边向左滑动超过左边的位置
                * 下边向上滑动不能超过上边的位置
                * 最小不能小于图片宽度的四分之一
                * */
                if((index > CropImageView.getPixelMapWidth()/8)
                        &&(CropImageView.getPositionLeft() < left)
                        &&(CropImageView.getPositionTop() < top)
                        &&(CropImageView.getPositionRight() > right)
                        &&(CropImageView.getPositionBottom() > bottom)){
                    //初始化滑动后的剪裁框
                    mScaledRect = new RectFloat(left,top,right,bottom);
                    //更新剪裁框
                    // @Param HighlightView clipBound util class
                    // @Param Component mClipBound your crop box
                    // @Param RectFloat mScaledRect a RectFloat which contain your crop box's position message
                    HighlightView.updateClipBound(clipBound,mClipBound, mScaledRect);
                }

            }

            @Override
            public void onScaleEnd(Component component, ScaleInfo scaleInfo) {
                if(mScaledRect != null){
                    CropImgAbility.mClipRect = mScaledRect;
                }
            }

        });
    }

    //获得裁剪框的宽
    public static float getClipBoundWidth(){
        return CropImgAbility.mClipRect.right - CropImgAbility.mClipRect.left;
    }

    //获得裁剪框的高
    public static float getClipBoundHeight(){
        return CropImgAbility.mClipRect.bottom - CropImgAbility.mClipRect.top;
    }

    //获得裁剪框的位置
    public static RectFloat getCropBoundPositionRect(){
        return new RectFloat(CropImgAbility.mClipRect.left , CropImgAbility.mClipRect.top ,
                CropImgAbility.mClipRect.right , CropImgAbility.mClipRect.bottom);
    }

    //裁剪框初始化，返回裁剪框的位置
    public static RectFloat getinit(Context context, int topIndex){
        //获取pixelmap的宽和高
        int pixelWidth = CropImageView.getPixelMapWidth();
        int pixelHeight = CropImageView.getPixelMapHeight();

        //屏幕大小
        int windowWidth = CropImageView.getWindowWidth();
        int windowHeight = CropImageView.getWindowHeight();

        /* 计算初始位置
        *  left为屏幕宽度的一半减去图片宽度的一半
        *  因为图片设置为水平居中
        *  top为传进来的值
        */
        int left = windowWidth/2 - pixelWidth/4;
        int top = topIndex + pixelHeight/2 - pixelWidth/4;
        int right = windowWidth/2 + pixelWidth/4;
        int bottom = topIndex + pixelHeight/2 + pixelWidth/4;

        RectFloat rectFloat = new RectFloat(left , top , right , bottom);

        return  rectFloat;
    }

}
