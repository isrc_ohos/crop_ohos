/*
 *    Copyright 2016 SoundCloud
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.soundcloud.android.crop;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

import java.util.ArrayList;

/*
 * This class isn't important.
 */
abstract class MonitoredAbilitySlice extends AbilitySlice {

    private final ArrayList<LifeCycleListener> listeners = new ArrayList<LifeCycleListener>();

    public static interface LifeCycleListener {
        public void onActivityInactived(MonitoredAbilitySlice activity);
        public void onActivityBackgrounded(MonitoredAbilitySlice activity);
        public void onActivityStarted(MonitoredAbilitySlice activity);
        public void onActivityStopped(MonitoredAbilitySlice activity);
    }

    public static class LifeCycleAdapter implements LifeCycleListener {
        public void onActivityInactived(MonitoredAbilitySlice activity) {}
        public void onActivityBackgrounded(MonitoredAbilitySlice activity) {}
        public void onActivityStarted(MonitoredAbilitySlice activity) {}
        public void onActivityStopped(MonitoredAbilitySlice activity) {}
    }

    public void addLifeCycleListener(LifeCycleListener listener) {
        if (listeners.contains(listener)) return;
        listeners.add(listener);
    }

    public void removeLifeCycleListener(LifeCycleListener listener) {
        listeners.remove(listener);
    }


    //onStart
    @Override
    protected void onInactive() {
        super.onInactive();
        for (LifeCycleListener listener : listeners) {
            listener.onActivityInactived(this);
        }
    }

    //onStop
    @Override
    protected void onBackground() {
        super.onBackground();
        for (LifeCycleListener listener : listeners) {
            listener.onActivityBackgrounded(this);
        }
    }

    //onCreate
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        for (LifeCycleListener listener : listeners) {
            listener.onActivityStarted(this);
        }
    }

    //onDestroy
    @Override
    protected void onStop() {
        super.onStop();
        for (LifeCycleListener listener : listeners) {
            listener.onActivityStopped(this);
        }
    }

}
