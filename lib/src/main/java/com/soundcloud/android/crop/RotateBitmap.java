/*
 *    Copyright 2016 SoundCloud
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.soundcloud.android.crop;

//import android.graphics.Bitmap;
//import android.graphics.Matrix;
import ohos.agp.utils.Matrix;
import ohos.media.image.PixelMap;

/*
 * This class isn't important.
 */
class RotateBitmap {

    private PixelMap pixelMap;
    private int rotation;

    public RotateBitmap(PixelMap pixelMap, int rotation) {
        this.pixelMap = pixelMap;
        this.rotation = rotation % 360;
    }

    public void setRotation(int rotation) {
        this.rotation = rotation;
    }

    public int getRotation() {
        return rotation;
    }

    public PixelMap getpixelMap() {
        return pixelMap;
    }

    public void setBitmap(PixelMap pixelMap) {
        this.pixelMap = pixelMap;
    }
}

