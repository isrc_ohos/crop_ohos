/*
 *    Copyright 2016 SoundCloud
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.soundcloud.android.crop;


import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.global.resource.*;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Rect;

import java.io.IOException;
import java.util.Optional;

/*
 * CropUtil is a class which can return a PixelMap just need an Id.
 */
public class CropUtil {

    public static Optional<PixelMap> getOriginalPixelMap(Context context, int id){
        String path = getPathById(context, id);
        if (path == null || path.isEmpty()) {
            return Optional.empty();
        }

        RawFileEntry assetManager = context.getResourceManager().getRawFileEntry(path);
        ImageSource.SourceOptions options = new ImageSource.SourceOptions();
        options.formatHint = "image/jpg";
        ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
        try {
            Resource asset = assetManager.openRawFile();
            ImageSource source = ImageSource.create(asset, options);
            return Optional.ofNullable(source.createPixelmap(decodingOptions));


        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    //获取裁剪后的图片
    public static Optional<PixelMap> getClippedPixelMap(Context context, int id){
        String path = getPathById(context, id);
        if (path == null || path.isEmpty()) {
            return Optional.empty();
        }
        //loading
        RawFileEntry assetManager = context.getResourceManager().getRawFileEntry(path);
        ImageSource.SourceOptions options = new ImageSource.SourceOptions();
        options.formatHint = "image/jpg";

        //decoding
        ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();

        //范围
        int marginLeft = CropImageView.getWindowWidth()/2 -CropImageView.getPixelMapWidth()/2;
        int marginTop = CropImageView.getPositionTop();
        int minX = (int) CropImgAbility.mClipRect.left-marginLeft;
        int minY = (int) CropImgAbility.mClipRect.top-marginTop;
        int width = (int) (CropImgAbility.mClipRect.right - CropImgAbility.mClipRect.left);
        int height = (int) (CropImgAbility.mClipRect.bottom - CropImgAbility.mClipRect.top);

        //转换
        Rect commonClipRect = new Rect( minX , minY , width , height );
        decodingOptions.desiredRegion = commonClipRect;

        try {
            Resource asset = assetManager.openRawFile();
            ImageSource source = ImageSource.create(asset, options);
            return Optional.ofNullable(source.createPixelmap(decodingOptions));


        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    //根据id获取相对路径
    public static String getPathById(Context context, int id){
        String path = "";
        if (context == null){
            return path;
        }

        ResourceManager manager = context.getResourceManager();
        if (manager == null){
            return path;
        }

        try {
            path = manager.getMediaPath(id);

        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return path;
    }

    public static Component getCropImageView(Context context , PixelMap pixelMap){
        //PixelMapHolder&Canvas
        PixelMapHolder PixelMapHolder = new PixelMapHolder(pixelMap);
        Paint mPaint = new Paint();
        //Component
        Component mPicture = new Component(context);
        //DrawTask
        Component.DrawTask drawPic = new Component.DrawTask() {
            @Override
            public void onDraw(Component component, Canvas canvas) {
                //draw图像
                canvas.drawPixelMapHolder(PixelMapHolder, 0, 0, mPaint);

                //component放大
                component.setScaleX(1f);
                component.setScaleY(1f);
            }
        };

        mPicture.addDrawTask(drawPic);
        return mPicture;
    }

    //裁剪
    public static void presentClippedPicture(Component component, RectFloat clipBound ,int source) {
        //new图片读取
        PixelMap clippedPixelMap = CropUtil.getClippedPixelMap(component.getContext(), source).get();
        //PixelMapHolder&Canvas
        PixelMapHolder clippedPixelMapHolder = new PixelMapHolder(clippedPixelMap);
        Paint paint = new Paint();

        Component.DrawTask clippedPicture = new Component.DrawTask() {
            @Override
            public void onDraw(Component component, Canvas canvas) {
                //draw图像
                canvas.drawPixelMapHolder(clippedPixelMapHolder,0,0, paint);
            }
        };

        component.addDrawTask(clippedPicture);
        component.setScaleX(1f);
        component.setScaleY(1f);

    }
}
