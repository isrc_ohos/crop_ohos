/*
 *    Copyright 2016 SoundCloud
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.soundcloud.android.crop;


import ohos.agp.components.AttrSet;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.media.image.PixelMap;

/*
*   这个类为一个工具类，用来储存被裁减图片的信息
*   储存被裁减图片的位图，以及被裁减图片的位置
*
*   提供了一些方法用来返回被裁减图片的宽度、高度、位置信息等等
*   @Author ZhaoBY
 */

public class CropImageView extends ImageViewTouchBase {

    private static PixelMap pixelMap;
    private static Context context;
    private static int top;

    public CropImageView(Context context) {
        super(context);
    }

    public CropImageView(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public CropImageView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    public void initPixelMap(PixelMap pixelMap , Context context , int top){
        this.pixelMap = pixelMap;
        this.context = context;
        this.top = top;
    }

    //返回图像宽度
    public static int getPixelMapWidth(){
        return pixelMap.getImageInfo().size.width;
    }

    //返回图像宽度
    public static int getPixelMapWidth(PixelMap pixelMap){
        return pixelMap.getImageInfo().size.width;
    }

    //返回图像高度
    public static int getPixelMapHeight(){
        return pixelMap.getImageInfo().size.height;
    }

    //返回图像高度
    public static int getPixelMapHeight(PixelMap pixelMap){
        return pixelMap.getImageInfo().size.height;
    }

    //返回屏幕宽度
    public static int getWindowWidth(){
        //获取手机屏幕大小
        DisplayManager displayManager = DisplayManager.getInstance();
        Display display = displayManager.getDefaultDisplay(context).get();
        DisplayAttributes displayAttributes = display.getAttributes();

        return displayAttributes.width;
    }

    //返回屏幕高度
    public static int getWindowHeight(){
        //获取手机屏幕大小
        DisplayManager displayManager = DisplayManager.getInstance();
        Display display = displayManager.getDefaultDisplay(context).get();
        DisplayAttributes displayAttributes = display.getAttributes();

        return displayAttributes.height;
    }

    //返回图片当前的left
    public static int getPositionLeft(){
        return getWindowWidth()/2 - getPixelMapWidth()/2;
    }
    //返回图片当前的top
    public static int getPositionTop(){
        return top;
    }

    //返回图片当前的Right
    public static int getPositionRight(){
        return getWindowWidth()/2 + getPixelMapWidth()/2;
    }

    //返回图片当前的top
    public static int getPositionBottom(){
        return top + getPixelMapHeight();
    }

    public static int getImageWidth(){
        return CropImgAbility.mPicture.getWidth();
    }

    //已弃用
    public static int getImageleft(){
        return getWindowWidth()/2 - getImageWidth()/2;
    }
    //已弃用
    public static int getImageTop(){
        return top;
    }
}
