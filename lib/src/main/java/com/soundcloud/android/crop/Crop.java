/*
 *    Copyright 2016 SoundCloud
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.soundcloud.android.crop;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.app.Context;


/**
 * Builder for crop Intents and utils for handling result
 * @Author ZhaoBY
 */
public class Crop {

    public static final int REQUEST_CROP = 6709;
    public static final int REQUEST_PICK = 9162;
    public static final int RESULT_ERROR = 404;
    public static final boolean CROPFLAG = true;
    public static final int CANCEL_CROP = 1;
    public static final int SUCCESS_CROP = 2;

    interface Extra {
        String ASPECT_X = "aspect_x";
        String ASPECT_Y = "aspect_y";
        String MAX_X = "max_x";
        String MAX_Y = "max_y";
        String AS_PNG = "as_png";
        String ERROR = "error";
    }

    private Intent cropIntent;

    /**
     * Create a crop Intent builder with source and destination image ID
     *
     * @param source      Id for image to crop
     * @param destination Id for saving the cropped image
     */
    public static Crop of(int source, int destination) {
        return new Crop(source, destination);
    }

    private Crop(int source, int destination) {
        cropIntent = new Intent();
        cropIntent.setParam("source",source);
        cropIntent.setParam("destination",destination);
    }

    /**
     * Crop area with fixed 1:1 aspect ratio
     */
    public Crop asSquare() {
        cropIntent.setParam(Extra.ASPECT_X, 1);
        cropIntent.setParam(Extra.ASPECT_Y, 1);
        return this;
    }

    /**
     * Send the crop Intent from an Activity
     *
     * @param abilitySlice Activity to receive result
     */
    public void start(Ability ability , AbilitySlice abilitySlice) {
        start(ability , abilitySlice, REQUEST_CROP);
    }

    /**
     * Send the crop Intent from an Activity with a custom request code
     *
     * @param abilitySlice    Activity to receive result
     * @param requestCode requestCode for result
     */
    public void start(Ability ability , AbilitySlice abilitySlice, int requestCode) {
        //给crop添加操作
        AbilitySlice cropImageAbilitySlice = new CropImgAbility();
        abilitySlice.presentForResult(cropImageAbilitySlice,getIntent(ability),requestCode);
    }

    /**
     * Get Intent to start crop Activity
     *
     * @param context Context
     * @return Intent for CropImageActivity
     */
    public Intent getIntent(Context context) {
        //增加额外数据
        cropIntent.setParam("resultImg", 0x1000003);
        return cropIntent;
    }

}
