/*
 *    Copyright 2016 SoundCloud
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.soundcloud.android.crop;

import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.bundle.AbilityInfo;
import ohos.global.resource.*;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.IOException;
import java.util.Optional;

import static ohos.agp.components.ComponentContainer.LayoutConfig.MATCH_PARENT;

/*
* @Author ZhaoBY
* This class named CropImgAbility extends an AbilitySlice.
* This AbilitySlice is already completed by author,
* you can use this abilityslice very easily like this:
*
*   public void start(){
*       AbilitySlice cropImageAbilitySlice = new CropImgAbility();
*       abilitySlice.present(cropImageAbilitySlice,getIntent(ability));
*   }
*
* This AbilitySlice provide a crop box which named mClipBound,
* and the crop box can move under your control which means that you can slide the crop box with your fingers.
* It also provide a Image to show the picture Which you would like to crop.
* And it also provide two buttons Which named "cancel" and "done".
 */
public class CropImgAbility extends MonitoredAbilitySlice {

    //定义此slice的Dependent布局
    private DependentLayout myLayout = new DependentLayout(this);
    //记录裁剪框大小的矩形
    public static RectFloat mClipRect;
    //被裁减的原图片
    //Component mPicture;
    public static Image mPicture;

    //被裁减的图片
    Component croppedImage;
    //裁剪框
    Component mClipBound;
    //裁剪框工具类
    HighlightView clipBound;
    //文件id
    int source;

    @Override
    public void onStart(Intent intent) {

        super.onStart(intent);
        //创建本slice的布局文件
        DependentLayout.LayoutConfig config = new DependentLayout.LayoutConfig(MATCH_PARENT, MATCH_PARENT);

        //设置默认竖屏
        setDisplayOrientation(AbilityInfo.DisplayOrientation.PORTRAIT);
        //设置布局的背景
        myLayout.setLayoutConfig(config);
        ShapeElement element = new ShapeElement();
        element.setRgbColor(new RgbColor(255, 255, 255));
        myLayout.setBackground(element);

        //设置button等属性
        setupViews();

        //加载裁剪框、背景图片等等
        loadInput(intent);

        //加载布局
        super.setUIContent(myLayout);
    }

    private void setupViews(){

        //创建取消button
        Button cancel = new Button(this);
        //为button增加布局条件
        DependentLayout.LayoutConfig cancelLayoutConfig = new DependentLayout.LayoutConfig();
        cancelLayoutConfig.setMargins(50,50,0,0);
        cancelLayoutConfig.addRule(DependentLayout.LayoutConfig.ALIGN_TOP);
        cancelLayoutConfig.addRule(DependentLayout.LayoutConfig.ALIGN_PARENT_LEFT);
        //设置背景颜色
        cancel.setLayoutConfig(cancelLayoutConfig);
        ShapeElement cancelElement = new ShapeElement();
        cancelElement.setRgbColor(new RgbColor(155, 155, 155));
        cancel.setBackground(cancelElement);
        //设置文本
        cancel.setText("Cancel");
        cancel.setTextSize(70);
        cancel.setHeight(180);
        cancel.setWidth(420);
        //绑定点击方法
        cancel.setClickedListener(new Component.ClickedListener() {
            public void onClick(Component v) {
                cancelToMainSlice();
            }
        });
        myLayout.addComponent(cancel);


        //创建剪裁button
        Button crop = new Button(this);
        //为button增加布局条件
        DependentLayout.LayoutConfig cropLayoutConfig = new DependentLayout.LayoutConfig();
        cropLayoutConfig.setMargins(0,50,50,0);
        cropLayoutConfig.addRule(DependentLayout.LayoutConfig.ALIGN_TOP);
        cropLayoutConfig.addRule(DependentLayout.LayoutConfig.ALIGN_PARENT_RIGHT);
        //设置背景颜色
        crop.setLayoutConfig(cropLayoutConfig);
        ShapeElement cropElement = new ShapeElement();
        cropElement.setRgbColor(new RgbColor(0, 125, 255));
        crop.setBackground(cropElement);
        //设置文本
        crop.setText("Done");
        crop.setTextSize(70);
        crop.setHeight(180);
        crop.setWidth(420);
        //绑定点击方法
        crop.setClickedListener(new Component.ClickedListener() {
            public void onClick(Component v) {
                onSaveClicked();
            }
        });
        myLayout.addComponent(crop);
    }

    //加载裁剪框等等
    private void loadInput(Intent intent){

        source = intent.getIntParam("source",0);

        //根据图片id获取pixelmap
        PixelMap pixelMapOriginal = CropUtil.getOriginalPixelMap(this,source).get();
        //初始化CropImageView,塞进去一个pixelMap
        CropImageView cropImageView = new CropImageView(this);
        cropImageView.initPixelMap(pixelMapOriginal , this , 400);

        //将mPicture实例化，并且塞进去pixelMap
        mPicture = new Image(this);
        mPicture.setPixelMap(pixelMapOriginal);
        //给mPicture增加布局
        DependentLayout.LayoutConfig componentLayoutConfig = new DependentLayout.LayoutConfig();
        componentLayoutConfig.setMargins(0,400,0,0); //边距
        componentLayoutConfig.addRule(DependentLayout.LayoutConfig.HORIZONTAL_CENTER); //水平居中
        mPicture.setLayoutConfig(componentLayoutConfig);
        myLayout.addComponent(mPicture);


        //初始化一个RectFloat用来记录裁剪框的初始位置
        mClipRect = HighlightView.getinit(this , 400);

        //设置裁剪框
        mClipBound = new Component(this);
        //加载剪裁框ClipBound
        clipBound = new HighlightView(this , mClipBound , mClipRect);
        mClipBound = clipBound.getClipbound(clipBound,mClipRect);
        myLayout.addComponent(mClipBound);

        //设置滑动监听
        clipBound.setSlideListener(clipBound,mClipBound);

        //设置缩放监听
        clipBound.setScaledListener(clipBound ,mClipBound);

    }

    //取消裁剪方法
    private void cancelToMainSlice(){
        Intent intent = new Intent();

        //增加裁剪状态及结果
        intent.setParam("cropFlag",!Crop.CROPFLAG);
        intent.setParam("cropStatus",1);


        // 通过Intent中的OperationBuilder类构造operation对象，指定设备标识（空串表示当前设备）、应用包名、Ability名称
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("com.example.android_crop_ohos")
                .withAbilityName("com.example.android_crop_ohos.MainAbility")
                .build();
        // 把operation设置到intent中
        intent.setOperation(operation);

        //跳转
        startAbility(intent);
    }

    //成功裁剪方法
    private void onSaveClicked(){

        //方法1:显示到MainActivity
        Intent intent = new Intent();

        //增加裁剪状态及结果
        intent.setParam("cropFlag",Crop.CROPFLAG);
        intent.setParam("cropStatus",2);


        //塞入裁剪结果
        intent.setParam("resultImg",source);
        intent.setParam("clipBoundLeft",mClipRect.left);
        intent.setParam("clipBoundTop",mClipRect.top);
        intent.setParam("clipBoundRight",mClipRect.right);
        intent.setParam("clipBoundBottom",mClipRect.bottom);

        // 通过Intent中的OperationBuilder类构造operation对象，指定设备标识（空串表示当前设备）、应用包名、Ability名称
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("com.example.android_crop_ohos")
                .withAbilityName("com.example.android_crop_ohos.MainAbility")
                .build();
        // 把operation设置到intent中
        intent.setOperation(operation);

        //跳转
        startAbility(intent);

        /*croppedImage = new Component(this);
        PixelMap crpppp = CropUtil.getClippedPixelMap(this,0x1000002,mClipRect).get();
        myLayout.removeComponent(mPicture);
        myLayout.removeComponent(mClipBound);
        Image image = new Image(this);
        DependentLayout.LayoutConfig imgLayoutConfig = new DependentLayout.LayoutConfig();
        imgLayoutConfig.setMargins(0,400,0,0);
        imgLayoutConfig.addRule(DependentLayout.LayoutConfig.ALIGN_TOP);
        imgLayoutConfig.addRule(DependentLayout.LayoutConfig.HORIZONTAL_CENTER);
        image.setLayoutConfig(imgLayoutConfig);
        image.setPixelMap(crpppp);
        myLayout.addComponent(image);*/

    }

    public static Optional<PixelMap> getOriginalPixelMap(Context context, int id){
        String path = getPathById(context, id);
        if (path == null || path.isEmpty()) {
            return Optional.empty();
        }

        RawFileEntry assetManager = context.getResourceManager().getRawFileEntry(path);
        ImageSource.SourceOptions options = new ImageSource.SourceOptions();
        options.formatHint = "image/jpg";
        ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
        try {
            Resource asset = assetManager.openRawFile();
            ImageSource source = ImageSource.create(asset, options);
            return Optional.ofNullable(source.createPixelmap(decodingOptions));


        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public static String getPathById(Context context, int id){
        String path = "";
        if (context == null){
            return path;
        }

        ResourceManager manager = context.getResourceManager();
        if (manager == null){
            return path;
        }

        try {
            path = manager.getMediaPath(id);

        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return path;
    }
}
